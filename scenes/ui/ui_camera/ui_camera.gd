extends Marker3D

# Mouse sensitivity
@export var sensitivity = 0.1
# Speed at which the camera angular movement decays
@export var speed_decay = 200
@export var zoom_rate = 2
@export var min_dist = 0.5
@export var max_dist = 100

# Camera node
@onready var _camera: Camera3D = $Camera

var _angle_sign := 1
var _angle := 0.
var _mouse_start_pos := Vector2.ZERO
var _mouse_distance := 0.

func _physics_process(delta):
	if Input.is_action_just_pressed("rotate_camera"):
		_mouse_start_pos = get_viewport().get_mouse_position()
	if Input.is_action_pressed("rotate_camera"):
		_handle_mouse_movement()
	elif Input.is_action_just_released("rotate_camera"):
		_handle_mouse_release()
	if Input.is_action_just_released("zoom"):
		_handle_zoom()
	elif Input.is_action_just_released("unzoom"):
		_handle_unzoom()
	else:
		_handle_speed_decay(delta)
	_rotate_camera(delta)

# Handle mouse movement when moving camera
func _handle_mouse_movement():
	Input.mouse_mode = Input.MOUSE_MODE_CONFINED_HIDDEN
	_mouse_distance = get_viewport().get_mouse_position().x - _mouse_start_pos.x
	_angle = _mouse_distance * sensitivity
	_angle_sign = sign(_angle)

# Put mouse back to original position and make it visible
func _handle_mouse_release():
	Input.warp_mouse(_mouse_start_pos)
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

# Handle camera angle speed decay when mouse is released
func _handle_speed_decay(delta):
	if _angle_sign < 0:
		_angle = min(_angle + speed_decay * delta, 0)
	else:
		_angle = max(_angle - speed_decay * delta, 0)

func _handle_zoom():
	var new_pos = _camera.position + (position - _camera.position).normalized() * zoom_rate
	if new_pos.distance_to(position) > min_dist:
		_camera.position = new_pos

func _handle_unzoom():
	var new_pos = _camera.position - (position - _camera.position).normalized() * zoom_rate
	if new_pos.distance_to(position) < max_dist:
		_camera.position = new_pos

# Rotate the camera around the 3D marker
func _rotate_camera(delta):
	rotate_y(-deg_to_rad(delta * _angle))
	_camera.look_at(position)
