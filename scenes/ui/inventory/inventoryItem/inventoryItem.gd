extends CharacterBody2D

class_name InventoryItem

@export var targetsCharacters: bool = false
@export var inWorldItem: PackedScene

signal playDropSound

var draggingDistance
var dir
var dragging
var newPosition = Vector2()

var duplicated = false
var mouse_in = false

@onready var ui_status = $/root/UIStatusSingleton
	
func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed() && mouse_in:
			# Duplicate when dragged
			if !duplicated:
				# update global ui status
				ui_status.held_item = self
				get_parent().add_child(duplicate());
				duplicated = true;
			draggingDistance = position.distance_to(get_viewport().get_mouse_position());
			dir = (get_viewport().get_mouse_position() - position).normalized();
			dragging = true;
			newPosition = get_viewport().get_mouse_position() - draggingDistance * dir;
		else:
			if duplicated:
				# Set the global ui status to inform of item dropping
				ui_status.held_item = null
				# Send playDropSound signal
				playDropSound.emit();
				queue_free();
			dragging = false;
			
	elif event is InputEventMouseMotion:
		if dragging:
			newPosition = get_viewport().get_mouse_position() - draggingDistance * dir;

func _physics_process(_delta):
	if dragging:
		velocity = (newPosition - position) * Vector2(30, 30);
		move_and_slide();
	
# Mouse entered and mouse exited signals
func mouse_entered():
	mouse_in = true;

func mouse_exited():
	mouse_in = false;
