extends Control
class_name Happinessbar

@onready var progressBar: ProgressBar = $"ProgressBar";
@onready var globalHappinessObserver: HappinessObserver = $"/root/GlobalHappinessObserver";

func _process(_delta):
	progressBar.value = globalHappinessObserver.happiness * 100;
