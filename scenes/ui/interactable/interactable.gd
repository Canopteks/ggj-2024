class_name Interactable

extends Area3D

signal just_hovered
signal stopped_hovering

var hovered : bool :
	set(value):
		if value == true and hovered == false:
			emit_signal("just_hovered")
		elif value == false and hovered == true:
			emit_signal("stopped_hovering")
		hovered = value
