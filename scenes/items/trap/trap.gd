extends Node3D
class_name Trap

@export var target_influence: float = -0.1;
@export var target_panic: bool = false;
@export var target_panic_speed: float = 400;
@export var target_panic_duration: float = 3;

@export var neighbors_influence : float = 0.02;
@export var neighbors_panic: bool = false;
@export var neighbors_panic_inward : bool = false;
@export var neighbors_panic_speed: float = 200;
@export var neighbors_panic_duration: float = 3;

@onready var effectZone: Area3D = $"EffectZone";

func _on_trigger_zone_body_entered(body):
	if !(body is Pignouf):
		return;
		
	# Influence the pignouf that triggered the trap
	var target_pignouf: Pignouf = body as Pignouf;
	target_pignouf.happiness += target_influence;
	if (target_panic):
		var direction3D = body.position - position;
		var direction2D = Vector2(direction3D.x, direction3D.z).normalized();
		target_pignouf.get_panicky(direction2D, target_panic_speed, target_panic_duration);
	
	# Influence the pignoufes around
	for neighbor in effectZone.get_overlapping_bodies():
		if !(neighbor is Pignouf):
			continue;
			
		var neighbor_pignouf: Pignouf = neighbor as Pignouf;
		# Don't influence the target twice
		if (neighbor_pignouf == target_pignouf):
			continue;
		neighbor_pignouf.happiness += neighbors_influence;
		if neighbors_panic:
			var vector_to_trap3D = position - neighbor_pignouf.position;
			var vector_to_trap2D = Vector2(vector_to_trap3D.x, vector_to_trap3D.z).normalized();
			vector_to_trap2D *= (1 if neighbors_panic_inward else -1);
			neighbor_pignouf.get_panicky(vector_to_trap2D, neighbors_panic_speed, neighbors_panic_duration);
		
	queue_free();
