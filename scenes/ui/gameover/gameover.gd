extends Node

var happinessObserver;
var soundPlayed = false;


# Called when the node enters the scene tree for the first time.
func _ready():
	# Get happinessObserver and subscribe to happiness_is_zero signal
	happinessObserver = get_node("/root/GlobalHappinessObserver");
	happinessObserver.connect("happiness_is_zero", self.showGameOverScreen);


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func showGameOverScreen():
	if !soundPlayed:
		get_node("AudioStreamPlayer").playing = true;
		soundPlayed = true;
		
	self.visible = true;

func restartGame():
	get_tree().change_scene_to_file("res://scenes/ui/loading_screen/loading_screen.tscn")
