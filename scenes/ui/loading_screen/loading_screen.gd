#Code from: https://www.gotut.net/loading-screen-in-godot-4/

extends Control

const target_scene_path = "res://scenes/main/main.tscn"

var loading_status : int

@onready var progress_bar : ProgressBar = $ProgressBar

func _ready() -> void:
	# Request to load the target scene:
	ResourceLoader.load_threaded_request(target_scene_path)
	
func _process(_delta: float) -> void:
	# Update the status:
	loading_status = ResourceLoader.load_threaded_get_status(target_scene_path)
	
	# Check the loading status:
	match loading_status:
		ResourceLoader.THREAD_LOAD_LOADED:
			# When done loading, change to the target scene:
			get_tree().change_scene_to_packed(ResourceLoader.load_threaded_get(target_scene_path))
		ResourceLoader.THREAD_LOAD_FAILED:
			# Well some error happend:
			print("Error. Could not load Resource")
