extends Character
class_name Pignouf

# Initial happiness
@export var initial_happiness : float = 0.5
@export var happiness_range : float = 0.05
# How much happiness degrades per second
@export var happiness_degradation: float = 0.01

@export var happiness_threshold: float = 0.5;
@export var sad_threshold: float = 0.1;

enum States {happy, sad, almost_dead, dead}
var state: States;

# Showing stuff
@onready var _distress_particles = $DistressParticles
@onready var _happiness_bar_sprite : Sprite3D = $HappinessBar
@onready var _happiness_subviewport : SubViewport = $HappinessBar/SubViewport
@onready var _happiness_bar : ProgressBar = $HappinessBar/SubViewport/ProgressBar
@onready var _particle_timer : Timer = $ParticleTimer
@onready var _bonus_particles : GPUParticles3D = $BonusParticles
@onready var _malus_particles : GPUParticles3D = $MalusParticles

# Sound effects
@onready var _sound_effects : AudioStreamPlayer = $/root/SoundEffects

# Current happiness level
var happiness : float :
	get:
		return happiness
	set(value):
		var diff = value - happiness
		if abs(diff) >= 0.02:
			if sign(diff) < 0:
				_malus_particles.emitting = true
				_bonus_particles.emitting = false
				_particle_timer.start()
			else:
				_bonus_particles.emitting = true
				_malus_particles.emitting = false
				_particle_timer.start()
		happiness = max(value, 0);

func _ready():
	super._ready()
	_happiness_bar_sprite.hide()
	happiness = initial_happiness + (randf()*2-1) * happiness_range;
	check_state();
	update_animation();

func _process(delta):
	super._process(delta)
	
	# update the happiness and check if something changed. If so, update the animation.
	var last_frame_state = state;
	happiness -= happiness_degradation * delta;
	_happiness_bar.value = happiness * 100
	check_state();
	if (state != last_frame_state):
		update_animation();

# update the happiness state of the pignouf
func check_state():
	if happiness > happiness_threshold:
		state = States.happy;
	elif happiness > sad_threshold:
		state = States.sad;
	elif happiness > 0:
		state = States.almost_dead;
	else:
		state = States.dead;

# update the animation cycle. careful because it starts it from the beginning
func update_animation():
	if (state == States.happy):
		_distress_particles.emitting = false
		_sprite.play("walk_happy");
	elif (state == States.sad):
		_distress_particles.emitting = false
		_sprite.play("walk_sad");
	elif (state == States.almost_dead):
		_distress_particles.emitting = true
		_sprite.play("walk_sad");
	elif (state == States.dead):
		_distress_particles.emitting = false
		_sprite.play("walk_dead");
		
func _on_interactable_just_hovered():
	super._on_interactable_just_hovered()
	if state == States.dead:
		_sound_effects.play()
	_happiness_subviewport.render_target_update_mode = SubViewport.UPDATE_ALWAYS
	_happiness_bar_sprite.show()

func _on_interactable_stopped_hovering():
	super._on_interactable_stopped_hovering()
	_happiness_bar_sprite.hide()
	_happiness_subviewport.render_target_update_mode = SubViewport.UPDATE_DISABLED


func _on_particle_timer_timeout():
	# Stop all particles
	_bonus_particles.emitting = false
	_malus_particles.emitting = false
