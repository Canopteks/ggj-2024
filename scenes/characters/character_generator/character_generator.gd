extends Node3D

@export var character_scenes : Array[PackedScene];
# Number of characters to spawn
@export_range(0, 500) var number_of_characters: int

# Min and max speed for characters
@export var min_speed = 30
@export var max_speed = 200

# The zone rectangle is used to determine spawn bounds
@onready var _zone := $Zone
var _bound_x : float
var _bound_z : float

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(min_speed <= max_speed, "Minimum character speed must be less or equal
	to max character speed")
	var zone_size = _zone.get_size()
	_bound_x = zone_size.x
	_bound_z = zone_size.z
	generate_characters()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

# Generate `number_of_characters` characters randomly in the spawning zone,
# with random speeds
func generate_characters():
	print_debug("generating %d characters" % number_of_characters)
	var bound_x_min = _zone.global_position.x - _bound_x/2
	var bound_x_max = _zone.global_position.x + _bound_x/2
	var bound_z_min = _zone.global_position.z - _bound_z/2
	var bound_z_max = _zone.global_position.z + _bound_z/2
	for i in number_of_characters:
		var coord_x = randf_range(bound_x_min, bound_x_max)
		var coord_z = randf_range(bound_z_min, bound_z_max)
		var new_charac = character_scenes.pick_random().instantiate()
		add_child(new_charac)
		new_charac.global_position.x = coord_x
		new_charac.global_position.z = coord_z
		new_charac.global_position.y = _zone.position.y
		new_charac.speed = randf_range(min_speed, max_speed)
