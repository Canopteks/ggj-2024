extends Area3D
class_name HappinessObserver

signal happiness_state_changed(happy: bool);
signal happiness_is_zero();

@export_range(0, 1) var happiness_threshold = 0.5;
@export var check_happiness_each_frame: bool = false;

var happiness : float = 0;
var pignoufs_around: int = 0;

func _ready():
	_update_happiness();
	
func _process(_delta):
	if check_happiness_each_frame:
		_update_happiness();

func _update_happiness():
	var new_happiness: float = 0;
	pignoufs_around = 0;
	for body in get_overlapping_bodies():
		if !(body is Pignouf):
			continue;
			
		var pignouf: Pignouf = body as Pignouf;
		pignoufs_around += 1;
		new_happiness += pignouf.happiness;
		
	# No one around: status quo
	if pignoufs_around == 0:
		return;
		
	new_happiness /= pignoufs_around;
	var happiness_status: bool = new_happiness>happiness_threshold;
	
	if (happiness_status != (happiness > happiness_threshold)):
		happiness_state_changed.emit(happiness_status);
	happiness = new_happiness;
	
	if happiness == 0:
		happiness_is_zero.emit();
	elif happiness > 1:
		happiness = 1;

# Pignouf collided, recompute the happiness
func _on_body_entered(body):
	if !(body is Pignouf):
		return;
		
	_update_happiness();

func _on_body_exited(body):
	if !(body is Pignouf):
		return;
		
	_update_happiness();
