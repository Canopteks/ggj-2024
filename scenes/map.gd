extends Node3D

@onready var _world_env : WorldEnvironment = $WorldEnvironment
@onready var _happiness_obs = $"/root/GlobalHappinessObserver"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var sat = lerpf(0.5, 1.5, _happiness_obs.happiness)
	_world_env.environment.adjustment_saturation = sat
