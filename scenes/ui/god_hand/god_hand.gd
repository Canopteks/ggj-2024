extends Node3D

const RAY_LENGTH = 1000

@export var camera_path : NodePath
@onready var _camera : Camera3D
@onready var _ui_status : UIStatus = $/root/UIStatusSingleton

@export var mouse_normal : Texture2D = load("res://assets/sprites/cusror/god-hand1.png")
@export var mouse_clicked : Texture2D = load("res://assets/sprites/cusror/god-hand2.png")

var _hovered_entity : Interactable = null

# Called when the node enters the scene tree for the first time.
func _ready():
	_camera = get_node(camera_path)
	_ui_status.connect("item_dropped", _handle_item_dropped)

func _physics_process(_delta):
	var cast_result = _cast_ray(2)
	if not cast_result.is_empty():
		var col = cast_result.collider
		if col is Interactable:
			_handle_hover(col)
	else:
		_handle_stop_hover()

# Handle clicking mouse
func _process(_delta):
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
		Input.set_custom_mouse_cursor(mouse_clicked)
	else:
		Input.set_custom_mouse_cursor(mouse_normal)

func _handle_hover(ent):
	if _hovered_entity != ent:
		_handle_stop_hover()
		_hovered_entity = ent
		_hovered_entity.hovered = true

func _handle_stop_hover():
	if _hovered_entity != null:
		_hovered_entity.hovered = false
		_hovered_entity = null

# Process item being dropped
func _handle_item_dropped(item):
	var result = _cast_ray(4)
	if not result.is_empty():
		# Instantiate the corresponding inworlditem of an inventory item
		if item.inWorldItem != null:
			var in_world_item = item.inWorldItem.instantiate()
			get_parent().add_child(in_world_item)
			in_world_item.position = result.position

func _cast_ray(layer):
	var space_state = get_world_3d().direct_space_state
	var mousepos = get_viewport().get_mouse_position()
	var origin = _camera.project_ray_origin(mousepos)
	var end = origin + _camera.project_ray_normal(mousepos) * RAY_LENGTH
	var query = PhysicsRayQueryParameters3D.create(origin, end, layer)
	query.collide_with_areas = true
	return space_state.intersect_ray(query)
