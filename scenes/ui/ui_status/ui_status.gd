class_name UIStatus

extends Node

# Signel emitted when the currently held item is dropped
signal item_dropped(item)

var held_item : InventoryItem = null :
	set(value):
		if held_item != null and value == null:
			emit_signal("item_dropped", held_item)
		held_item = value

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
