extends Influencer
class_name Goose

var target_pignouf: Pignouf = null;
@onready var aggro_timer: Timer = $"AggroTimer";

func _ready():
	super._ready();
	select_target();
	aggro_timer.start();
	
func _process(delta):
	super._process(delta);
	if (target_pignouf != null):
		_dir = (target_pignouf.position - position).normalized();

func select_target():
	var pignoufs_around: Array[Pignouf];
	for neighbor in effectZone.get_overlapping_bodies():
		if !(neighbor is Pignouf):
			continue;
		pignoufs_around.append(neighbor as Pignouf);
		
	if (pignoufs_around.size() == 0):
		target_pignouf = null;
		
	target_pignouf = pignoufs_around.pick_random();
	aggro_timer.start();

func _on_aggro_timer_timeout():
	select_target();
