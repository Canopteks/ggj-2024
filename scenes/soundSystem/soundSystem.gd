extends Node

@export var MUSIC_NEUTRAL: AudioStreamPlayer;
@export var MUSIC_SAD: AudioStreamPlayer;
@export var MUSIC_HAPPY: AudioStreamPlayer;

var happinessObserver

# Called when the node enters the scene tree for the first time.
func _ready():
	# Check if every exported variable is set
	if MUSIC_NEUTRAL == null:
		push_error("Please set the variable MUSIC_NEUTRAL");
	if MUSIC_SAD == null:
		push_error("Please set the variable MUSIC_SAD");
	if MUSIC_HAPPY == null:
		push_error("Please set the variable MUSIC_HAPPY");
		
	# Instanciate happinessObserver to get happiness value
	happinessObserver = get_node("/root/GlobalHappinessObserver");


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	updateVolume();

# We balance sad and happy volume depending on the happiness meter.
# The more it is close to 100, the more happy is loud (disabled under 60 happiness)
# The more it is close to 0, the more sad is loud (disabled above 40 happiness
# When happinessPercentage is at 50, we can only hear neutral
func updateVolume():
	if happinessObserver.happiness >= 0.4 and happinessObserver.happiness <= 0.6:
		MUSIC_SAD.volume_db = -80;
		MUSIC_HAPPY.volume_db = -80;
	elif happinessObserver.happiness >= 0.6:
		MUSIC_HAPPY.volume_db = 0.5 * (happinessObserver.happiness * 100) - 50;
	elif happinessObserver.happiness <= 0.4:
		MUSIC_SAD.volume_db = -0.5 * (happinessObserver.happiness * 100);
