extends Node

@onready var audioPlayer: AudioStreamPlayer = $"AudioStreamPlayer";

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

# Plays item drop sound when called
func playItemDropSound():
	audioPlayer.playing = true;
