extends Character
class_name Influencer

@export var influence : float = 0.02;
@export var panic: bool = false;
@export var panic_inward : bool = false;
@export var panic_speed = 200;
@export var panic_duration = 0.5;
@export var effect_period = 0.5;

@onready var effectZone: Area3D = $"EffectZone";
@onready var effectTimer: Timer = $"EffectTimer";

func _ready():
	super._ready();
	effectTimer.wait_time = effect_period;
	effectTimer.start();

func _influence_around():
	# Influence the pignoufes around
	for neighbor in effectZone.get_overlapping_bodies():
		if !(neighbor is Pignouf):
			continue;
			
		# Affect the happiness around
		var pignouf: Pignouf = neighbor as Pignouf;
		pignouf.happiness += influence;
		
		# Affect the movements too with panic
		if panic:
			var vector_to_trap3D = position - pignouf.position;
			var vector_to_trap2D = Vector2(vector_to_trap3D.x, vector_to_trap3D.z).normalized();
			vector_to_trap2D *= (1 if panic_inward else -1);
			pignouf.get_panicky(vector_to_trap2D, panic_speed, 0.5);
			
	effectTimer.start();
	
