extends Influencer
class_name DavidDuck

var time_to_die: bool = false;

func die():
	_sprite.play("death");
	time_to_die = true;
	speed = 0;
	influence = 0;
	panic = false;

func _on_sprite_animation_finished():
	if time_to_die:
		queue_free();
