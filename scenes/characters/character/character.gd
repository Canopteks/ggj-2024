extends CharacterBody3D
class_name Character

@export var plane_height := 0
@export var min_dir_change_time = 0
@export var max_dir_change_time = 5
@export var speed = 100

@onready var _stroll_timer : Timer = $"StrollTimer"
var _dir := Vector3.ZERO

@onready var _rush_timer : Timer = $"RushTimer"
var _currently_rushing: bool = false
var _temporary_speed: float;

# Procedural animation
@onready var _sprite : AnimatedSprite3D = $Sprite
@onready var _original_sprite_pos = _sprite.position
# Height to which the sprite will "jump" when hovered
const JUMP_HEIGHT = 0.5
# Duration of jump
const JUMP_DURATION := 0.2
# Modulation color for when the sprite is hovered
@export var hover_color := Color(0.8, 0.8, 1., 0.95)

# Handle height
@onready var _raycast : RayCast3D = $RayCast3D


# Called when the node enters the scene tree for the first time.
func _ready():
	assert(min_dir_change_time < max_dir_change_time, "Minimum direction change time
	must be smaller than maximum direction change time")
	_stroll_timer.wait_time = _choose_dir_change_time()
	_stroll_timer.start()
	_change_dir()

func _physics_process(_delta):
	_handle_sprite_flip()
	_handle_y_pos()
#	position.y = plane_height

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_move(delta)
	_handle_borders()

# Choose random time to change direction
func _choose_dir_change_time():
	return randf_range(min_dir_change_time, max_dir_change_time)

# Change character direction randomly
func _change_dir():
	_dir = Vector3(randf_range(-1, 1), 0, randf_range(-1, 1)).normalized()

# Handle movement
func _move(delta):
	velocity = speed * _dir * 0.02
	move_and_slide()

# Handle map borders by detecting collisions with static bodies and going
# opposite to the collision point
func _handle_borders():
	var last_col = get_last_slide_collision()
	if last_col != null and last_col.get_collider() is StaticBody3D:
		_dir = (position - last_col.get_position()).normalized()

# On timer timeout, change direction, chose a random time and restart the timer
func _on_stroll_timer_timeout():
	if !(_currently_rushing):
		_change_dir()
	_stroll_timer.wait_time = _choose_dir_change_time()
	_stroll_timer.start()

# Make the character rushes in a direction for a duration
func get_panicky(direction: Vector2, rush_speed: float, duration: float):
	_currently_rushing = true;
	
	# Start the panick timer
	_rush_timer.wait_time = duration;
	_rush_timer.start();
	
	_temporary_speed = speed;
	speed = rush_speed;
	_dir = Vector3(direction.x, 0, direction.y).normalized();

func _handle_sprite_flip():
	var cam = get_viewport().get_camera_3d()
	# Magic formula, thank you Thomas, it works
	_sprite.flip_h = _dir.dot(cam.global_transform.basis.z.cross(Vector3.UP)) < 0

func _handle_y_pos():
	if _raycast.is_colliding():
		position.y = _raycast.get_collision_point().y

func _on_rush_timer_timeout():
	_currently_rushing = false;
	speed = _temporary_speed;
	_change_dir();

func _on_interactable_just_hovered():
	# Use a tween for some cut procedural animation
	var new_pos = _original_sprite_pos + Vector3(0, JUMP_HEIGHT, 0)
	var tween = get_tree().create_tween()
	tween.tween_property(_sprite, "position", new_pos, JUMP_DURATION/2)
	tween.tween_property(_sprite, "position", _original_sprite_pos, JUMP_DURATION/2)
	# Also change the color of the sprite slightly to indicate which character
	# is hovered
	_sprite.modulate = hover_color

func _on_interactable_stopped_hovering():
	_sprite.modulate = Color.WHITE
